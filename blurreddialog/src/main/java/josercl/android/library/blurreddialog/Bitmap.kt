package josercl.android.library.blurreddialog

import android.content.Context
import android.graphics.Bitmap
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur

fun Bitmap.fastBlur(context: Context, radius: Float): Bitmap? {
    val outputBitmap = Bitmap.createBitmap(this)
    val rs = RenderScript.create(context)

    val input = Allocation.createFromBitmap(rs, this)
    val output = Allocation.createFromBitmap(rs, outputBitmap)
    val script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
    script.setRadius(radius)
    script.setInput(input)
    script.forEach(output)
    output.copyTo(outputBitmap)

    return outputBitmap
}