package josercl.android.library.blurreddialog

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.josercl_blurreddialog_layout.*

private const val DEFAULT_RADIUS = 20f

open class BlurredDialog : DialogFragment() {
    @LayoutRes
    protected var innerLayout: Int = 0
    protected lateinit var title: String
    protected var message: String? = null
    protected var cancelableOnBackgroundClick = true
    protected var blurRadius: Float = DEFAULT_RADIUS
    protected var showOverlay = true

    protected var positiveButtonText: String? = null
    protected var positiveButtonOnClickListener: View.OnClickListener? = null
    protected var negativeButtonText: String? = null
    protected var negativeButtonOnClickListener: View.OnClickListener? = null
    protected var neutralButtonText: String? = null
    protected var neutralButtonOnClickListener: View.OnClickListener? = null

    protected var positiveButton: MaterialButton? = null
    protected var negativeButton: MaterialButton? = null
    protected var neutralButton: MaterialButton? = null

    private lateinit var innerView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.josercl_blurreddialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val layoutInflater = LayoutInflater.from(requireContext())
        val mainView = layoutInflater.inflate(R.layout.josercl_blurreddialog_layout, container, false)
        val innerContainer = mainView.findViewById<FrameLayout>(R.id.inner_container)
        innerView = layoutInflater.inflate(innerLayout, innerContainer, false)
        innerContainer.addView(innerView)
        return mainView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.positiveButton = positive_btn
        this.negativeButton = negative_btn
        this.neutralButton = neutral_btn

        if (positiveButtonText == null) {
            positive_btn.visibility = View.GONE
        } else {
            positive_btn.text = positiveButtonText
            positive_btn.setOnClickListener { v ->
                positiveButtonOnClickListener?.onClick(v)
                dismiss()
            }
        }

        if (negativeButtonText == null) {
            negative_btn.visibility = View.GONE
        } else {
            negative_btn.text = negativeButtonText
            negative_btn.setOnClickListener { v ->
                negativeButtonOnClickListener?.onClick(v)
                dismiss()
            }
        }

        if (neutralButtonText == null) {
            neutral_btn.visibility = View.GONE
        } else {
            neutral_btn.text = neutralButtonText
            neutral_btn.setOnClickListener { v ->
                neutralButtonOnClickListener?.onClick(v)
                dismiss()
            }
        }

        if (showOverlay){
            dialog_background.setBackgroundColor(
                ResourcesCompat.getColor(requireContext().resources, R.color.black_overlay, requireContext().theme)
            )
        }

        dialog_title.visibility = if (title.isNotEmpty()) View.VISIBLE else View.GONE
        dialog_title.text = title

        if (cancelableOnBackgroundClick) {
            dialog_background.setOnClickListener { dismiss() }
        }

        onInnerViewCreated(innerView, savedInstanceState)
    }

    open fun onInnerViewCreated(view: View, savedInstanceState: Bundle?) {
        if (message != null && innerLayout == R.layout.josercl_blurreddialog_default) {
            innerView.findViewById<TextView>(R.id.blurred_dialog_message).text = message
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.run {
            this.window?.run {
                val bitmap = requireActivity().takeScreenshot()
                if (bitmap != null) {
                    val blurredBackground = bitmap.fastBlur(requireContext(), blurRadius)
                    setBackgroundDrawable(BitmapDrawable(resources, blurredBackground))
                }
            }
        }
    }

    class Builder(private val context: Context) {
        private var showOverlay: Boolean = true
        fun setShowOverlay(show: Boolean): Builder {
            this.showOverlay = show
            return this
        }

        private var blurRadius: Float = DEFAULT_RADIUS
        fun setBlurRadius(@FloatRange(from = 1.0) radius: Float): Builder {
            this.blurRadius = radius
            return this
        }

        private var layout: Int = R.layout.josercl_blurreddialog_default
        fun setLayout(@LayoutRes layout: Int): Builder {
            this.layout = layout
            return this
        }

        private var positiveButtonText: String? = null
        private var positiveButtonOnClickListener: View.OnClickListener? = null
        fun setPositiveButton(@StringRes text: Int, listener: ((v: View) -> Unit)? = null): Builder =
            setPositiveButton(context.getString(text), View.OnClickListener { v -> listener?.invoke(v) })

        fun setPositiveButton(@StringRes text: Int, listener: View.OnClickListener?): Builder =
            setPositiveButton(context.getString(text), listener)

        fun setPositiveButton(text: String, listener: ((v: View) -> Unit)? = null): Builder =
            setPositiveButton(text, View.OnClickListener { v -> listener?.invoke(v) })

        fun setPositiveButton(text: String, listener: View.OnClickListener?): Builder {
            this.positiveButtonText = text
            this.positiveButtonOnClickListener = listener
            return this
        }

        private var negativeButtonText: String? = null
        private var negativeButtonOnClickListener: View.OnClickListener? = null
        fun setNegativeButton(@StringRes text: Int, listener: ((v: View) -> Unit)? = null): Builder =
            setNegativeButton(context.getString(text), View.OnClickListener { v -> listener?.invoke(v) })

        fun setNegativeButton(@StringRes text: Int, listener: View.OnClickListener?): Builder =
            setNegativeButton(context.getString(text), listener)

        fun setNegativeButton(text: String, listener: ((v: View) -> Unit)? = null): Builder =
            setNegativeButton(text, View.OnClickListener { v -> listener?.invoke(v) })

        fun setNegativeButton(text: String, listener: View.OnClickListener?): Builder {
            this.negativeButtonText = text
            this.negativeButtonOnClickListener = listener
            return this
        }

        private var neutralButtonText: String? = null
        private var neutralButtonOnClickListener: View.OnClickListener? = null
        fun setNeutralButton(@StringRes text: Int, listener: ((v: View) -> Unit)? = null): Builder =
            setNeutralButton(context.getString(text), View.OnClickListener { v -> listener?.invoke(v) })

        fun setNeutralButton(@StringRes text: Int, listener: View.OnClickListener?): Builder =
            setNeutralButton(context.getString(text), listener)

        fun setNeutralButton(text: String, listener: ((v: View) -> Unit)? = null): Builder =
            setNeutralButton(text, View.OnClickListener { v -> listener?.invoke(v) })

        fun setNeutralButton(text: String, listener: View.OnClickListener?): Builder {
            this.neutralButtonText = text
            this.neutralButtonOnClickListener = listener
            return this
        }

        private var title: String? = null
        fun setTitle(@StringRes text: Int) = this.setTitle(context.getString(text))
        fun setTitle(text: String): Builder {
            this.title = text
            return this
        }

        private var message: String? = null
        fun setMessage(@StringRes text: Int) = setMessage(context.getString(text))
        fun setMessage(text: String): Builder {
            this.message = text
            return this
        }

        private var cancelableOnBackground = true
        fun setCancelable(cancelable: Boolean): Builder {
            this.cancelableOnBackground = cancelable
            return this
        }

        fun build(): BlurredDialog {
            if (layout == 0) {
                throw RuntimeException("You must specify the layout")
            }
            return BlurredDialog().apply {
                showOverlay = this@Builder.showOverlay
                blurRadius = this@Builder.blurRadius
                message = this@Builder.message
                innerLayout = this@Builder.layout
                cancelableOnBackgroundClick = this@Builder.cancelableOnBackground
                title = this@Builder.title ?: ""
                positiveButtonText = this@Builder.positiveButtonText
                positiveButtonOnClickListener = this@Builder.positiveButtonOnClickListener
                negativeButtonText = this@Builder.negativeButtonText
                negativeButtonOnClickListener = this@Builder.negativeButtonOnClickListener
                neutralButtonText = this@Builder.neutralButtonText
                neutralButtonOnClickListener = this@Builder.neutralButtonOnClickListener
            }
        }
    }

}