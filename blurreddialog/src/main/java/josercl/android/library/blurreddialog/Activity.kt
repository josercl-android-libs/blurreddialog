package josercl.android.library.blurreddialog

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Point
import android.graphics.Rect
import androidx.core.view.drawToBitmap

fun Activity.takeScreenshot(): Bitmap? {
    val b1: Bitmap = this.window.decorView.rootView.drawToBitmap()

    val frame = Rect()
    this.window.decorView.getWindowVisibleDisplayFrame(frame)
    val size = Point()
    this.windowManager.defaultDisplay.getSize(size)
    val width = size.x
    val height = size.y

    return Bitmap.createBitmap(b1, 0, 0, width, Math.min(height + 2 * frame.top, b1.height))
}