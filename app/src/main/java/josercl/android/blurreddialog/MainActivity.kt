package josercl.android.blurreddialog

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import josercl.android.library.blurreddialog.BlurredDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dialog = BlurredDialog.Builder(this)
            .setLayout(R.layout.dialog_layout)
            .setPositiveButton("Aceptar") { Log.d("WTF", "Positive Button") }
            .setNegativeButton("Cancelar") { Log.d("WTF", "Negative Button") }
            .setNeutralButton("Ni ni") { Log.d("WTF", "Neutral Button") }
            .setTitle("Hey Listen")
            .setCancelable(false)
            .build()

        btn.setOnClickListener {
            dialog.show(supportFragmentManager, null)
        }
    }
}
